const express = require('express');// call express
const router  = express.Router();                 // define our app using express
const role = require('../models/role');
const User = require('../models/user');
const validator = require('validator');
router.route('/users')
    .get((req,res)=>{
        User.find().select('-password').exec((err, users)=>{
            if(err){
                console.log(err);
                res.status(404).send(err);
            }
            else
            res.json(users);
        });

    })

    .post((req, res)=> {
        let user = new User(req.body);
        user.save((err, user) => {
            if (err) {
                res.status(500).send(err);
            }
            else {
                res.json(user);
            }
        })
    })
    .delete((req, res)=>{
        User.remove({},(err, result)=>{
            if(err){
                res.status(500).send(err);
            }
            else
            res.status(200).json(result);
        })
    });

router.route('/users/:id')
    .get((req, res)=>{
        User.findById(req.params.id).populate('role').select('-password').exec((err, user)=>{
          if(err)
              res.status(500).send(err);
          else
          res.json(user);
        })
    })
    .delete((req,res)=>{
        User.remove({_id:req.params.id},(err,result)=>{
            if(err)
                res.status(500).send(err);
else
            res.json(result);
        })
    })
    .put((req,res)=> {
        User.findByIdAndUpdate({_id: req.params.id}, req.body, {new: true, runValidators: true}, (err, user) => {
            if (err)
                res.status(500).send(err);
            else if (!user)
                res.status(404).json({message: "the specified user could not be found."});
            else
                res.status(200).json(user);
        })
    });

router.route('/users/requestpassword')
    .post((req, res)=>{
        const emailAddress = req.body.email;
        console.log(emailAddress);
        if(!validator.isEmail(emailAddress)){
            res.status(400).json({
                message: 'Invalid Email Address'
            });
        }
        else{
            const generator = require('generate-password');
            let password = generator.generate({
                length: 10,
                numbers: true
            });
            // Update password in users table:
            User.find({email: emailAddress}).exec((err, users)=> {
                if (err || !users || users.length<=0) {
                    res.status(500).send(err || "User not found");
                }
                else {
                    // console.log(user);
                    const crypto = require('crypto-js');
                    User.findByIdAndUpdate({_id: users[0]._id}, {password: crypto.SHA256(password).toString()}, {new: true})
                        .exec((err, user) => {
                            if (err || !user) {
                                res.status(500).send(err || "User not found after updating new password");
                            }
                            else {
                                console.log(user);
                                // Send Mail to user:
                                res.mailer.send('email', {
                                    to: emailAddress, // REQUIRED. This can be a comma delimited string just like a normal email to field.
                                    subject: `Request Password`, // REQUIRED.
                                    password: password, // All additional properties are also passed to the template as local variables.
                                    lastName: user.lastName,
                                    firstName: user.firstName,
                                    email: user.email
                                }, function (err) {
                                    if (err) {
                                        // handle error
                                        console.log(err);
                                        res.status(500).send('There was an error sending the email');
                                    }
                                    else
                                        res.json(user);
                                });
                            }
                        })
                }
            })

        }
    });

module.exports = router;