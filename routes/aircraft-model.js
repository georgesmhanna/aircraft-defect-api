const express    = require('express');        // call express
const router        = express.Router();                 // define our app using express

const AircraftModel = require('../models/aircraft-model');

router.route('/aircraftmodels')
.get((req, res)=>{
    AircraftModel.find().exec((err, aircraftModels)=>{
        if(err)
            res.status(500).send(err);
else
        res.json(aircraftModels);
    })
})
    .post((req, res)=>{
        let aircraftModel = new AircraftModel(req.body);
        aircraftModel.save((err, aircraftModel)=>{
            if (err){
                res.status(500).send(err);
            }
            else{
                res.json(aircraftModel);
            }
        })
    });

router.route('/aircraftmodels/:id')
    .get((req, res)=>{
        AircraftModel.findById(req.params.id).exec((err, aircraftModel)=>{
            if(err)
                res.status(500).send(err);
            else
            res.json(aircraftModel);
        })
    })
    .delete((req,res)=>{
        AircraftModel.remove({_id:req.params.id},(err,result)=>{
            if(err)
                res.status(500).send(err);
else
            res.json(result);
        })
    })
    .put((req,res)=>{
        AircraftModel.findByIdAndUpdate({_id: req.params.id}, req.body, {new: true, runValidators: true}, (err, aircraftModel)=>{
            if(err)
                res.status(500).send(err);
            else if(!aircraftModel)
                res.status(404).json({message: "the specified aircraft model could not be found."});
            else
            res.status(200).json(aircraftModel);
        })
    });

module.exports = router;