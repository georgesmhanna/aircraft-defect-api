const express    = require('express');// call express
const app = express();
const router  = express.Router();                 // define our app using express
const AircraftSection = require('../models/aircraft-section');
const base64Img = require('base64-img');

router.route('/aircraftsections')
    .get((req,res)=> {
        AircraftSection
            .find()
            // .populate(
            //     {
            //         path: 'aircraft',
            //         populate: {path: 'model'}
            //     })
            // .populate('defectProfile')
            // .populate('parent')
            .exec((err, aircraftSections) => {
                if (err) {
                    console.log(err);
                    res.status(404).send(err);
                } else
                res.json(aircraftSections);
            });

    })

    .post((req, res)=> {
        let aircraftSection = new AircraftSection(req.body);
        aircraftSection.save((err, aircraftSection) => {
            if (err) {
                res.status(500).send(err);
                // console.log(err);
            }
            else {
                    res.json(aircraftSection);
            }
        })
    })
    .delete((req, res)=>{
        AircraftSection.remove({},(err, result)=>{
            if(err){
                res.status(500).send(err);
            } else
            res.status(200).json(result);
        })
    });

router.route('/aircraftsections/:id')
    .get((req, res)=>{
        AircraftSection
            .findById(req.params.id)
            // .populate('aircraft')
            // .populate('defectProfile')
            // .populate('parent')
            .exec((err, aircraftSection)=>{
            if(err)
                res.status(500).send(err);
            else
                res.json(aircraftSection);
        })
    })
    .delete((req,res)=>{
        AircraftSection.remove({_id:req.params.id},(err,aircraftSection)=>{
            if(err)
                res.status(500).send(err);
else
            res.json({message: `${aircraftSection.n} aircraft Sections successfully deleted.`});
        })
    })
    .put((req,res)=> {
        if(req.body.parent===null || req.body.parent==='')
            req.body.parent = undefined;
        AircraftSection.findByIdAndUpdate({_id: req.params.id}, req.body, {new: true, runValidators: true}, (err, aircraftSection) => {
            if (err)
                res.status(500).send(err);
            else if (!aircraftSection)
                res.status(404).json({message: "the specified aircraft section could not be found."});
            else
                res.status(200).json(aircraftSection);
        })
    });

router.route('/aircraftsections/getImage64/:name')
    .get((req, res)=>{
        base64Img.base64(`./assets/${req.params.name}.png`,
            (err, data)=>{
                if(err){
                    res.status(500).send(err);
                }
                else{
                    res.json(data);
                }
            })
    });

module.exports = router;