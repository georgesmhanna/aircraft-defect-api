const express = require('express');// call express
const app = express();
const router = express.Router();                 // define our app using express
const Defect = require('../models/defect');
const path = require('path');
const base64Img = require('base64-img');
const moment = require('moment-timezone');
router.route('/defects')
    .get((req, res) => {
        Defect
            .find()
            .sort('-reportedDate')
            .exec((err, defects) => {
                if (err) {
                    res.status(404).send(err);
                }
                else {
                    for (let defect of defects) {
                        try {
                            defect.isImage = !!base64Img.base64Sync(`./defect-images/defect-${defect._id}.png`);
                        } catch (err) {
                            defect.isImage = false;
                        }

                    }
                    res.json(defects);
                }

            });

    })

    .post((req, res) => {
        if (req.body.reporter === null || req.body.reporter === '')
            req.body.reporter = undefined;
        if (req.body.closedBy === null || req.body.closedBy === '')
            req.body.closedBy = undefined;
        let defect = new Defect(req.body);
        Defect.count({
            location: (defect.location) ? defect.location.toLowerCase() : defect.location,
            aircraft: defect.aircraft,
            section: defect.section,
            defectType: defect.defectType,
            closedDate: undefined
        }).exec((err, count) => {
            if (err) {
                res.status(500).send(err);
            }
            else {
                if (count !== 0) {
                    res.status(400).send('Cannot insert duplicate defect');
                }
                else {
                    defect.reportedDate = new Date();
                    defect.save((err, defect) => {
                        if (err) {
                            res.status(500).send(err);
                        }
                        else {
                            res.json(defect);
                        }
                    })
                }
            }
        });
    })
    .delete((req, res) => {
        Defect.remove({}, (err, result) => {
            if (err) {
                res.status(500).send(err);
            }
            else
                res.status(200).json(result);
        })
    });

router.route('/defects/:id')
    .get((req, res) => {
        Defect
            .findById(req.params.id)
            .exec((err, defect) => {
                if (err)
                    res.status(500).send(err);
                else{
                    try {
                        defect.isImage = !!base64Img.base64Sync(`./defect-images/defect-${defect._id}.png`);
                    } catch (err) {
                        defect.isImage = false;
                    }
                    res.json(defect);
                }
            })
    })
    .delete((req, res) => {
        Defect.remove({_id: req.params.id}, (err, defect) => {
            if (err)
                res.status(500).send(err);
            else
                res.json({message: `${defect.n} defects successfully deleted.`});
        })
    })
    .put((req, res) => {
        if (req.body.reporter === null || req.body.reporter === '')
            req.body.reporter = undefined;
        if (req.body.closedBy === null || req.body.closedBy === '')
            req.body.closedBy = undefined;

        let newLocation = req.body.location;
        let newAircraft = req.body.aircraft;
        let newSection = req.body.section;
        let newDefectType = req.body.defectType;
        let newIsImage = req.body.isImage;


        Defect.count({
            location: (newLocation) ? newLocation.toLowerCase() : newLocation,
            aircraft: newAircraft,
            section: newSection,
            isImage: newIsImage,
            defectType: newDefectType,
            closedDate: undefined,
            _id: {
                $ne: req.params.id
            }
        }).exec((err, count) => {
            if (err) {
                res.status(500).send(err);
            }
            else {
                if (count !== 0) {
                    res.status(400).send('Cannot insert duplicate defect');
                }
                else {

                    Defect.findByIdAndUpdate({_id: req.params.id}, req.body, {
                        new: true,
                        runValidators: true
                    }, (err, defect) => {
                        if (err)
                            res.status(500).send(err);
                        else if (!defect)
                            res.status(404).json({message: "the specified defect could not be found."});
                        else
                            res.status(200).json(defect);
                    })
                }
            }
        });
    });

router.route('/defects/getCountBySection/:id')
    .get((req, res) => {
        Defect.findById(req.params.id)
            .exec((err, defect) => {
                if (err) {
                    res.status(404).send("Defect not found");
                }
                else {
                    const sectionId = defect.section;
                    const aircraftId = defect.aircraft;

                    Defect.count({
                        aircraft: aircraftId,
                        section: sectionId,
                        closedDate: null
                    })
                        .exec((err1, count) => {
                            if (err1) {
                                res.status(500).send("Could not get count of unclosed defects");
                            }
                            else {
                                res.json(count);
                            }
                        })
                }
            })
    });

router.route('/defects/upload/:id')
    .post((req, res) => {
        if (!req.files)
            return res.status(400).send('No files were uploaded.');

        let image = req.files.image;

        image.mv(`./defect-images/defect-${req.params.id}.png`, (err) => {
            if (err)
                return res.status(500).send(err);

            res.send('File uploaded!');
        });
    });

router.route('/defects/getImage64/:id')
    .get((req, res) => {
        base64Img.base64(`./defect-images/defect-${req.params.id}.png`,
            (err, data) => {
                if (err) {
                    res.status(500).send(err);
                }
                else {
                    res.json(data);
                }
            })
    });

router.route('/defects/isImage/:id')
    .get((req, res) => {
        base64Img.base64(`./defect-images/defect-${req.params.id}.png`,
            (err, data) => {
                if (err) {
                    res.json(false);
                }
                else {
                    res.json(true);
                }
            })
    });

router.route('/getDefectGroups')
    .get((req, res) => {
        let offsetCETmillisec = moment.tz.zone('Asia/Beirut').utcOffset(moment()) * 60 * 1000;
        Defect.aggregate([
            {
                "$group": {
                    _id: {
                        week:
                            {$week: [{$subtract: ['$reportedDate', offsetCETmillisec]}]},
                        aircraft: "$aircraft"
                    },
                    // opened: {$sum: {$cond: [{$ne: ["$closedDate", null]}, 1, 0]}},

                    opened: {$sum: {$cond: {if: {$gt: ["$closedDate", moment('1/1/1900')]}, then: 0, else: 1}}},
                    closed: {$sum: {$cond: {if: {$gt: ["$closedDate", moment('1/1/1900')]}, then: 1, else: 0}}},

                }
            }
        ]).exec((err, results) => {
            if (err)
                return res.status(500).send(err);
            let formattedResults = results
                .sort((group1, group2) =>
                    group1._id.week > group2._id.week)
                .map(group => {
                    return {
                        "Week": group._id.week,
                        "aircraft": group._id.aircraft,
                        "Opened": group.opened,
                        "Closed": group.closed
                    };
                });
            Defect.populate(formattedResults, {path: 'aircraft', select: 'name registrationNo'}, function (err, populatedDefects) {
                if (err)
                    return res.status(500).send(err);
                res.send(populatedDefects
                    .map(group => {
                        return {
                            "Week": group.Week,
                            "Aircraft": group.aircraft.name,
                            "Registration No": group.aircraft.registrationNo,
                            "Opened": group.Opened,
                            "Closed": group.Closed
                        };

                    })
                );
            });
        })
    });

module.exports = router;