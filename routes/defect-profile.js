const express    = require('express');// call express
const app = express();

const router  = express.Router();                 // define our app using express

const DefectProfile = require('../models/defect-profile');


router.route('/defectprofiles')
    .get((req,res)=>{
        DefectProfile.find().exec((err, defectProfiles)=>{
            if(err){
                console.log(err);
                res.status(404).send(err);
            }
            else
            res.json(defectProfiles);
        });

    })

    .post((req, res)=> {
        let defectProfile = new DefectProfile(req.body);
        defectProfile.save((err, defectProfile) => {
            if (err) {
                res.status(500).send(err);
                // console.log(err);
            }
            else {
                res.json(defectProfile);
            }
        })
    })
    .delete((req, res)=>{
        DefectProfile.remove({},(err, result)=>{
            if(err){
                res.status(500).send(err);
            }
            else
            res.status(200).json(result);
        })
    });

router.route('/defectprofiles/:id')
    .get((req, res)=>{
        DefectProfile.findById(req.params.id).exec((err, defectProfile)=>{
            if(err)
                res.status(500).send(err);
            else
            res.json(defectProfile);
        })
    })
    .delete((req,res)=>{
        DefectProfile.remove({_id:req.params.id},(err,result)=>{
            if(err)
                res.status(500).send(err);

           else  res.json(result);
        })
    })
    .put((req,res)=>{

        DefectProfile.findByIdAndUpdate({_id: req.params.id}, req.body, {new: true, runValidators: true}, (err, defectProfile)=>{
            if(err)
                res.status(500).send(err);
        else    if(!defectProfile)
                res.status(404).json({message: "the specified user could not be found."});
else
            res.status(200).json(defectProfile);
        })
    });


module.exports = router;