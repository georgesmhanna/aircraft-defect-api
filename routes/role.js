const express    = require('express');        // call express
const router        = express.Router();                 // define our app using express
const Role = require('../models/role');

router.route('/roles')
    .get((req,res)=>{
        Role.find().exec((err, roles)=>{
            if(err){
                console.log(err);
                res.status(404).send(err);
            }
            else
            res.json(roles);
        });

    })

    .post((req, res)=>{
        let role = new Role(req.body);
        role.save((err, role)=>{
            if (err){
                res.status(500).send(err);
            }
            else{
                res.json(role);
            }
        })
    });

router.route('/roles/:id')
    .get((req, res)=>{
        Role.findById(req.params.id, (err, role)=>{
            if(err)
                res.status(500).send(err);
            else
            res.json(role);
        })
    })
    .delete((req,res)=>{
       Role.remove({_id:req.params.id},(err,result)=>{
            if(err)
                res.status(500).send(err);
else
            res.json(result);
        })
    })
    .put((req,res)=>{

        Role.findByIdAndUpdate({_id: req.params.id}, req.body, {new: true, runValidators: true}, (err, role)=>{

            if(err)
                res.status(500).send(err);
            else
            if(!role)
                res.status(404).json({message: "the specified role could not be found."});
            else
            res.status(200).json(role);
        })
    });


module.exports = router;