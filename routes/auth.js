const express = require('express');// call express
const router  = express.Router();
const role = require('../models/role');
const User = require('../models/user');
const generator = require('jsonwebtoken');


router.route('/auth/login')
.post((req, res)=> {
    User.find({
        email: req.body.email,
        password: req.body.password
    }).exec((err, users) => {
        if (err) {
            res.status(500).send(err);
        }

        else if(!users || users.length<=0)
            res.status(404).json({message: "User not found. Invalid Credentials"});
        else {
            console.log(users);
            // const loggedInUser = users.filter(user => user.role.name === 'admin')[0];
            const loggedInUser = users[0];//.filter(user => user.role.name === 'admin')[0];
                // update last logged in:
                User.findByIdAndUpdate({_id: loggedInUser._id}, {lastLogin: new Date()}, {new: true})
                    .populate('role')
                    .exec((err, user) => {
                        if (err)
                            res.status(500).send(err);
                        else if (!user) {
                            res.status(404).json({message: "the specified user could not be fetched after updating last login."});
                        }
                        else {
                            res.json({
                                data: {
                                    user: user,
                                    token: generator.sign({ user: user }, 'shhhhh', {expiresIn: '1h'})
                                }
                            });
                        }
                    })


        }
    });
});

router.route('/auth/logout')
    .post((req, res)=>{
    res.send(true);
});
// router.route('/auth/register')
//     .post((req, res)=>{
//         let user = new User(req.body);
//
//         user.save((err, user) => {
//             if (err) {
//                 res.status(500).send(err);
//             }
//             else {
//
//             res.json(
//                 {
//                     data:
//                         {
//                             user: user,
//                             // token: generator.sign({ user: user }, 'shhhhh')
//                         }
//                 }
//                 );
//             }
//         });
//     });

module.exports = router;