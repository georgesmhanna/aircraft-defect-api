const express    = require('express');// call express
const app = express();
const router  = express.Router();                 // define our app using express
const Aircraft = require('../models/aircraft');

router.route('/aircrafts')
    .get((req,res)=>{
        Aircraft.find().exec((err, aircrafts)=>{
            if(err){
                console.log(err);
                res.status(404).send(err);
            }
            else
            res.json(aircrafts);
        });

    })

    .post((req, res)=> {
        let aircraft = new Aircraft(req.body);
        aircraft.createdOn = new Date();
        aircraft.save((err, aircraft) => {
            if (err) {
                res.status(500).send(err);
                // console.log(err);
            }
            else {
                res.json(aircraft);
            }
        })
    })
    .delete((req, res)=>{
        Aircraft.remove({},(err, result)=>{
            if(err){
                res.status(500).send(err);
            }
            else
            res.status(200).json(result);
        })
    });

router.route('/aircrafts/:id')
    .get((req, res)=> {
        Aircraft.findById(req.params.id).exec((err, aircraft) => {
            if (err)
                res.status(500).send(err);
            else
            res.json(aircraft);
        })
    })
    .delete((req,res)=>{
        Aircraft.remove({_id:req.params.id},(err,result)=>{
            if(err)
                res.status(500).send(err);
else
            res.json(result);
        })
    })
    .put((req,res)=>{

        Aircraft.findByIdAndUpdate({_id: req.params.id}, req.body, {new: true, runValidators: true}, (err, aircraft)=>{
            if(err)
                res.status(500).send(err);

            else if(!aircraft)
                res.status(404).json({message: "the specified aircraft could not be found."});
            else res.status(200).json(aircraft);
        })
    });


module.exports = router;