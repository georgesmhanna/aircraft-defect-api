const express    = require('express');// call express
const app = express();

const router  = express.Router();                 // define our app using express


const DefectType = require('../models/defect-type');

router.route('/defecttypes')
    .get((req,res)=>{
        DefectType.find().exec((err, defectTypes)=>{
            if(err){
                console.log(err);
                res.status(404).send(err);
            }
            else
            res.json(defectTypes);
        });

    })

    .post((req, res)=> {
        let defectType = new DefectType(req.body);
        defectType.save((err, defectType) => {
            if (err) {
                res.status(500).send(err);
                // console.log(err);
            }
            else {

                    res.json(defectType);

            }
        })
    })
    .delete((req, res)=>{
        DefectType.remove({},(err, result)=>{
            if(err){
                res.status(500).send(err);
            }
            else
            res.status(200).json(result);
        })
    });

router.route('/defecttypes/:id')
    .get((req, res)=>{
        DefectType.findById(req.params.id).exec((err, defectType)=>{
            if(err)
                res.status(500).send(err);
            else res.json(defectType);
        })
    })
    .delete((req,res)=>{
        DefectType.remove({_id:req.params.id},(err,defectType)=>{
            if(err)
                res.status(500).send(err);
else
            res.json({message: `${defectType.n} defect Types successfully deleted.`});
        })
    })
    .put((req,res)=>{

        DefectType.findByIdAndUpdate({_id: req.params.id}, req.body, {new: true, runValidators: true}, (err, defectType)=>{
            if(err)
                res.status(500).send(err);
             else if(!defectType)
                res.status(404).json({message: "Specified defect type could not be found"});
            else
            res.status(200).json(defectType);
        })
    });


module.exports = router;