const express    = require('express');        // call express
const app        = express();                 // define our app using express
const bodyParser = require('body-parser');
const mailer = require('express-mailer');
const morgan = require('morgan');
const mongoose = require('mongoose');
const config = require('./config');
const cors = require('cors');
const docs = require("express-mongoose-docs");
const fileUpload = require('express-fileupload');

// const mongoUrl = 'mongodb://georgesmhanna:ol1234@ds119060.mlab.com:19060/georges-mhanna-test';
const mongoUrl = config.mongoUrl; 'mongodb://localhost/test';

mailer.extend(app, config.mailerConf );

// mongoose.connect(mongoUrl, {config: { autoIndex: false } })
mongoose.connect(mongoUrl)
.then(()=> { console.log(`Successfully Connected to the Mongodb Database  at URL : ${mongoUrl}`)})
.catch((err)=> { console.log(`Error Connecting to the Mongodb Database at URL : ${mongoUrl}, err = ${err}`)});

mongoose.connection.on('error', console.error.bind(console, 'connection error:'));

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(fileUpload());

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

const port = process.env.PORT || 3000;        // set our port

let roleRouter = require('./routes/role');
let userRouter = require('./routes/user');
let aircraftModelRouter = require('./routes/aircraft-model');
let aircraftRouter = require('./routes/aircraft');
let defectProfileRouter = require('./routes/defect-profile');
let defectTypeRouter = require('./routes/defect-type');
let aircraftSectionRouter = require('./routes/aircraft-section');
let defectRouter = require('./routes/defect');
let authRouter = require('./routes/auth');


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    next();
});

app.use('/api', roleRouter);
app.use('/api', userRouter);
app.use('/api', aircraftModelRouter);
app.use('/api', aircraftRouter);
app.use('/api', defectProfileRouter);
app.use('/api', defectTypeRouter);
app.use('/api', aircraftSectionRouter);
app.use('/api', defectRouter);
app.use('/api', authRouter);
docs(app, mongoose);


app.use((req, res, next)=>{
    res.status(404).send('not found');
    console.log('route not found');
});


app.listen(port);
console.log('Server connected on port ' + port);