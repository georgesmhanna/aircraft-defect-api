const validator =require('validator');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const DefectTypeSchema = new Schema({
        name: {
            type: String,
            required: true,
            unique: true
        },

        defectProfile: {
            type: Schema.Types.ObjectId,
            ref: 'defectProfile'
        }
    },
    {
        timestamps: true
    });


module.exports = mongoose.model('defectType', DefectTypeSchema);
