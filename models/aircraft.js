const validator =require('validator');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AircraftSchema = new Schema({
        name: {
            type: String,
            required: true,
            unique: false
        },
        createdOn: {
            type: Date,
            required: true
        },

        model: {
            type: Schema.Types.ObjectId,
            ref: 'aircraftModel'
        },
        registrationNo: {
            type: String
        }
    },
    {
        timestamps: true
    });


module.exports = mongoose.model('aircraft', AircraftSchema);
