const validator =require('validator');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let isValidEmail = email=>{
    return validator.isEmail(email);
};

const UserSchema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        validate: [isValidEmail, 'Email not valid'],
        lowercase: true,
        trim: true
    },
    password: {
        type: String,
        select: false
    },
    lastLogin: Date,
    role: {
        type: Schema.Types.ObjectId,
        ref: 'role'
    }
},
    {
        timestamps: true
    });

UserSchema.virtual('name').get(function () {
    return this.firstName + ' ' + this.lastName;
});

UserSchema.post('save', function(error, doc, next) {
    if (error.name === 'MongoError' && error.code === 11000) {
        next(new Error('There was a duplicate key error'));
    } else {
        next(error);
    }
});
module.exports = mongoose.model('user', UserSchema);
