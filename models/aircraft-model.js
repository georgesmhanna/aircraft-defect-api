const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const AircraftModelSchema = new Schema({
        name: {
            type: String,
            required: true,
            unique: true
        },
        registrationNo: {
            type: String,
            required: true
        }
    },
    {
        timestamps: true
    });


module.exports = mongoose.model('aircraftModel', AircraftModelSchema);