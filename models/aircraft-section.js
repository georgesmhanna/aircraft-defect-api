const validator =require('validator');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AircraftSectionSchema = new Schema({

        name: {
            type: String,
            required: true,
            unique: true
        },

        aircraft: {
            type: Schema.Types.ObjectId,
            ref: 'aircraft'
        } ,

        parent: {
            type: Schema.Types.ObjectId,
            ref: 'aircraftSection',
            required: false
        },

        defectProfile: {
            type: Schema.Types.ObjectId,
            ref: 'defectProfile'
        }
    },
    {
        timestamps: true
    });


module.exports = mongoose.model('aircraftSection', AircraftSectionSchema);
