const validator =require('validator');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const DefectProfileSchema = new Schema({
        name: {
            type: String,
            required: true,
            unique: true
        }
    },
    {
        timestamps: true
    });


module.exports = mongoose.model('defectProfile', DefectProfileSchema);
