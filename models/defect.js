const validator = require('validator');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DefectSchema = new Schema({
        reporter: {
            type: Schema.Types.ObjectId,
            ref: 'user',
            required: false
        },
        reportedDate: {
            type: Date
        },
        aircraft: {
            type: Schema.Types.ObjectId,
            ref: 'aircraft'
        },
        defectType: {
            type: Schema.Types.ObjectId,
            ref: 'defectType'
        },
        section: {
            type: Schema.Types.ObjectId,
            ref: 'aircraftSection'
        },
        comment: String,
        closedBy: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        },
        closedDate: Date,
        location: {
            type: String,
            trim: true,
            lowercase: true
        },
        isImage: {
            type: Boolean,
            required: false
        },
        partNumber: {
            type: String,
            trim: true,
        },
        workOrderNumber: {
            type: String,
            trim: true,
        },
        issueDate: {
            type: String,
            trim: true,
        }
    },
    {
        timestamps: true
    });


module.exports = mongoose.model('defect', DefectSchema);
